//
//  ProductPersistenceTest.swift
//  SmartValue
//
//  Created by Dwayne Ong on 30/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import XCTest

class ProductPersistenceTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    
        
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("SavedProduct") == nil {
            let products : [String] = ["Kicap", "Banana"]
            defaults.setObject(products, forKey: "SavedProduct")
            defaults.synchronize()
        }
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testReadingArray() {
        
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        if let products = defaults.objectForKey("SavedProduct") as? [String]{
            XCTAssert(products.count > 0, "Should have product list contain some items")
            
        }else{
           XCTAssert(false, "Unable to read fruit list")
        }

    }
    
    func testReadingFromDefault() {
        
        let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject("Ainor", forKey: "SavedNamed")
        defaults.synchronize()
        let name: String? = defaults.stringForKey("SavedNamed")
        XCTAssertEqual("Ainor", name, "Saved named should be match")
        
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
