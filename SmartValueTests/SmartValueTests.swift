//
//  SmartValueTests.swift
//  SmartValueTests
//
//  Created by Jasri Samad on 16/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import XCTest
@testable import SmartValue

class SmartValueTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSaveToFile() {
        var products: [Product] = []
        let product: Product = Product.createRandomProduct()
        products.append(product)
        products.append(product)
        products.append(product)
        
        ProductLoader.sharedLoader.saveProductsToFile(products)
    }
    
    func testLoadingAPI() {
        
        let expectation = self.expectationWithDescription("Load Task")
        
        let url: NSURL = NSURL(string: "https://api.backendless.com/v1/data/Products")!
        
        //session config
        let sessionConfig: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfig.HTTPAdditionalHeaders = ["application-id": "444AAEB0-7C53-0CD0-FF3C-05F4FC6EB300","secret-key": "472AD44A-47BC-75E8-FFF1-C68B8C2F7700"]
        
        let session: NSURLSession = NSURLSession(configuration: sessionConfig)
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            XCTAssert(error == nil, "something went wrong, error: \(error?.localizedDescription)")
            
            if let httpResponse: NSHTTPURLResponse = response as? NSHTTPURLResponse {
                XCTAssert(httpResponse.statusCode == 200, "Status code should be 200, actual \(httpResponse.statusCode)")
            }else {
                XCTAssert(false, "no http response")
            }
            
            if let data = data {
                let responseString: String? = String(data: data, encoding: NSUTF8StringEncoding)
                print(responseString)
            }
            
            //convert data to JSON
            if let responseJSON: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject] {
                
                //read the data Array from the returned JSON
                let dataArray: [[String:AnyObject]] = responseJSON["data"] as! [[String:AnyObject]]
                
                //go tru json
                for productJSON: [String:AnyObject] in dataArray {
                    //find name
                    let name: String = productJSON["productName"] as! String
                    print("found JSON data for \(name)")
    
                }
                
            }
            
            expectation.fulfill()
        }
        
        loadTask.resume()
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }

    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
   
        
     
    
      /*  let testProduct: [Product] = Product.generateFakeProducts()
        // this to test product has 5 products
        XCTAssert(testProduct.count == 5, "Yes, theres 5 products")
 */
    }
    
    

    


