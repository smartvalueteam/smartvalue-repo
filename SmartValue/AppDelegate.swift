//
//  AppDelegate.swift
//  SmartValue
//
//  Created by Jasri Samad on 16/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit
import BarcodeScanner


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var productListVC: ProductListViewController?
    
    
    // FOR USING BACKENDLESS SDK
    
    let APP_ID = "444AAEB0-7C53-0CD0-FF3C-05F4FC6EB300"
    let SECRET_KEY = "472AD44A-47BC-75E8-FFF1-C68B8C2F7700"
    let VERSION_NUM = "v1"
    
    var backendless = Backendless.sharedInstance()
    
    /////////////////////////////
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        backendless.initApp(APP_ID, secret:SECRET_KEY, version:VERSION_NUM)
        
        // If you plan to use Backendless Media Service, uncomment the following line (iOS ONLY!)
        // backendless.mediaService = MediaService()
        
        if let navigationController: UINavigationController = window?.rootViewController as? UINavigationController {
            if let productListVC = navigationController.viewControllers.first as? ProductListViewController {
                self.productListVC = productListVC
                
            }
        }
        
        //productListVC?.products = ProductLoader.sharedLoader.loadProductsFromServer()
        //productListVC?.products = ProductLoader.sharedLoader.readProductsFromFile()
        
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        if let productListVC = self.productListVC {
//            ProductLoader.sharedLoader.saveProductsToFile(productListVC.products)
//        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

