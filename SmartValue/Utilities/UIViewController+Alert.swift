//
//  UIViewController+Alert.swift
//  SmartValue
//
//  Created by Jason Khong on 8/13/16.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, okButtonTitle: String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: okButtonTitle, style: .Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

extension UITableViewCell {
    func showAlert(title: String, message: String, okButtonTitle: String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: okButtonTitle, style: .Default, handler: nil))
        
        //UITableViewCell cannot present VC. Use root view controller
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
}

extension UICollectionViewCell {
    func showAlert(title: String, message: String, okButtonTitle: String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: okButtonTitle, style: .Default, handler: nil))
        
        //UITableViewCell cannot present VC. Use root view controller
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
}


