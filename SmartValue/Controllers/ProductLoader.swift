//
//  ProductLoader.swift
//  SmartValue
//
//  Created by Jasri Samad on 01/08/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class ProductLoader {
    static let sharedLoader: ProductLoader = ProductLoader()
    private init() {
        let config: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = [
            "application-id": "444AAEB0-7C53-0CD0-FF3C-05F4FC6EB300",
            "secret-key": "472AD44A-47BC-75E8-FFF1-C68B8C2F7700",
            "application-type": "REST"
        ]
        
        self.session = NSURLSession(configuration: config)
        self.baseURL = NSURL(string: "https://api.backendless.com/v1/")!
    }
    
    var backendless = Backendless.sharedInstance()
    
    //Manage data files
    let fileManager: NSFileManager = NSFileManager.defaultManager()
    
    //manage loading from server
    let session: NSURLSession
    let baseURL: NSURL
    
    func dataFileURL() -> NSURL {
        let docDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = docDirectory.URLByAppendingPathComponent("data.plist")
        
        return filePath
        
    }
    
    func readProductsFromFile() -> [Product] {
        var products: [Product] = []
        
        if self.dataFileURL().checkResourceIsReachableAndReturnError(nil) {
            //check if datafile exist before loading it
            let productData: [NSData] = NSArray(contentsOfURL: self.dataFileURL()) as! [NSData]
            
            for data in productData {
                let product: Product = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Product
                products.append(product)
            }
        }
        
        return products
    }
    
    func saveProductsToFile(products: [Product]) {
        var productsData: [NSData] = []
        for product in products {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(product)
            productsData.append(data)
        }
        (productsData as NSArray).writeToURL(self.dataFileURL(), atomically: true)
    }
    
    
}

extension ProductLoader {
    func loadProductsFromServer(offset: Int = 0, productBarCode: String?, productNameSearch: String?, completionBlock:((products: [Product],offset: Int, error: NSError?) -> Void)?){
        var stringURL: String = ""
        var resultOffset: Int = 0

        if productBarCode != "" {
            stringURL = "%20and%20Barcode%3D" + productBarCode!
        } else if productNameSearch != "" {
            let trimmedProductName: String = productNameSearch!.stringByReplacingOccurrencesOfString(" ", withString: "")
            stringURL = "%20and%20productName%20like%20%27%25\(trimmedProductName)%25%27"
            
        }
        
        let url: NSURL = NSURL(string: "data/Products?pageSize=10&offset=\(offset)&sortBy=productName%20asc&where=productName%20is%20not%20null\(stringURL)", relativeToURL: self.baseURL)!
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            // Check for network error
            if let error = error {
                completionBlock?(products: [], offset: resultOffset, error: error)
                return
            }
            
            var json: [String : AnyObject]!
            do {
                if let data = data {
                    json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
                }
            } catch let jsonError {
                completionBlock?(products: [], offset: resultOffset, error: jsonError as NSError)
                return
            }
            
            var productsFromServer: [Product] = []
            if let resultOffsetJSON: Int = json["offset"] as? Int {
                resultOffset = resultOffsetJSON
            }

            if let resultsArray: [ [String:AnyObject] ] = json["data"] as? [ [String:AnyObject] ] {
                for jsonDict in resultsArray {
                    let product: Product = Product(json: jsonDict)!
                    
                    productsFromServer.append(product)
                }
            }
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                completionBlock?(products: productsFromServer, offset: resultOffset, error: nil)
            })
            
            
        }
        
        loadTask.resume()
        //return products
        
    }
    
    func loadProductPriceListFromServer(barCode: String,sortBy: String, completionBlock:((priceList: [ProductPriceList], error: NSError?) -> Void)?){
        
        var sortParameter = ""
        
        switch sortBy {
        case "price":
            sortParameter = "productPrice%20asc"
        default:
            sortParameter = "created%20desc"
        }
        
        let url: NSURL = NSURL(string: "data/Products?sortBy=\(sortParameter)&where=Barcode%3D\(barCode)", relativeToURL: self.baseURL)!
        
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            
            // Check for network error
            if let error = error {
                completionBlock?(priceList: [], error: error)
                return
            }
            
            var json: [String : AnyObject]!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            } catch let jsonError {
                completionBlock?(priceList: [], error: jsonError as NSError)
                return
            }
            
            var productListFromServer: [ProductPriceList] = []
            
            if let resultsArray: [ [String:AnyObject] ] = json["data"] as? [ [String:AnyObject] ] {
                for jsonDictPricelist in resultsArray {
                    let product: ProductPriceList = ProductPriceList(json: jsonDictPricelist)!
                    
                    productListFromServer.append(product)
                }
            }
            dispatch_async(dispatch_get_main_queue(), {() -> Void in
                completionBlock?(priceList: productListFromServer, error: nil)
            })
            
            
        }
        
        loadTask.resume()
        //return products
        
    }
    
    func timeStamp() -> Int {
        // function that return unix timestamp
        let timestamp = Int(NSDate().timeIntervalSince1970)
        return timestamp
    }
    
    
    func loadImageTask(imageURL: NSURL, completionBlock:((image: UIImage!, error: NSError?) -> Void)?) -> NSURLSessionDataTask {
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: imageURL)
        urlRequest.cachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
        
        let task: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (imageData: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            // Check for network error
            if let error = error {
                completionBlock?(image: UIImage(named: "noimagefound"), error: error)
                return
            }
            
            var image: UIImage! = nil
            if let imageData = imageData {
                image = UIImage(data: imageData)
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completionBlock?(image: image, error: error)
                
            })
        }
        
        return task
    }

    
    func saveProductsOnServer(product: Product, successBlock: ((succeeded: Bool, error: NSError?) -> Void)?) {
        if product.objectId == nil {
            createProductOnServer(product, successBlock: successBlock)
        } else {
            updateProductOnServer(product, successBlock: successBlock)
        }
    }
    
    func addPriceOnServer(price: ProductPriceList, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        let url: NSURL = NSURL(string: "data/Products", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonDict: [String:AnyObject] = price.jsonDictPricelist() //["productName": product.name, "productPrice": product.price ?? NSNull()]
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            // Check for network error
            if let error = error {
                successBlock?(succeeded: false, error: error)
                return
            }
            
            //let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
//            
//            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
//            let json : [String: AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String: AnyObject]
            
            //price.productObjectId = (json["objectId"] as? String)!
            
            successBlock?(succeeded: true, error: nil)
            //NSLog("HTTP \(httpCode): \(jsonString!)")
            
        }
        postTask.resume()
    }

    
    func createProductOnServer(product: Product, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        let url: NSURL = NSURL(string: "data/Products", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonDict: [String:AnyObject] = product.jsonDict() //["productName": product.name, "productPrice": product.price ?? NSNull()]
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(jsonDict, options: NSJSONWritingOptions(rawValue: 0))
        
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json : [String: AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String: AnyObject]
            
            product.objectId = json["objectId"] as? String
            
            successBlock?(succeeded: true, error: nil)
            NSLog("HTTP \(httpCode): \(jsonString!)")
            
        }
        postTask.resume()
    }
    
    func updateProductOnServer(product: Product, successBlock:((succeeded: Bool, error: NSError?) -> Void)?) {
        let url: NSURL = NSURL(string: "data/Products/\(product.objectId!)", relativeToURL: self.baseURL)!
        
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let jsonData: NSData = try! NSJSONSerialization.dataWithJSONObject(product.jsonDict(), options: NSJSONWritingOptions(rawValue: 0))
        urlRequest.HTTPBody = jsonData
        
        let postTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(urlRequest) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            let jsonString: String? = String(data: data!, encoding: NSUTF8StringEncoding)
            let json : [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            product.objectId = json["objectId"] as? String
            NSLog("HTTP \(httpCode): \(jsonString!)")
        }
        postTask.resume()
    }
    func createImageOnServerUsingBESDK(product: Product) {
        print("\n*** Uploading files with the Backendless ASYNC API START ***")
        
        let imageData = UIImageJPEGRepresentation(product.photo!, 0.1)
        let newfilename = product.name.stringByReplacingOccurrencesOfString(" ", withString: "") + "_\(timeStamp())"
        
        backendless.fileService.upload(
            "products/\(newfilename).jpg",
            content: imageData,
            overwrite:true,
            response: { ( uploadedFile : BackendlessFile!) -> () in
                print("createImageOnServer: File has been uploaded. File URL is - \(uploadedFile.fileURL)")
                product.photoUrl = NSURL(string: uploadedFile.fileURL )!
                
                self.saveProductsOnServer(product, successBlock: { (succeeded, error) in
                    print("product saved")
                })
            },
            error: { ( fault : Fault!) -> () in
                print("createImageOnServer: Server reported an error: \(fault)")
        })
    }
    
    func createImageOnServer(image: UIImage, completionBlock:((fileURL: NSURL?, error: NSError?)->Void)?) {
        let url: NSURL = NSURL(string: "files/products/photo.jpg?overwrite=true", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        //urlRequest.addValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("REST", forHTTPHeaderField: "application-type")
        
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        
        let postTask: NSURLSessionUploadTask = self.session.uploadTaskWithRequest(urlRequest, fromData: imageData) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Check HTTP code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            // Receive file url and name
            let json : [String : AnyObject]!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            } catch let jsonError {
                completionBlock?(fileURL: nil, error: jsonError as NSError)
                return
            }
            
            let fileUrlString: String = json["url"] as! String
            let fileURL: NSURL = NSURL(string: fileUrlString)!
            completionBlock?(fileURL: fileURL, error: nil)
            
            NSLog("HTTP \(httpCode): \(json)")
        }
        
        postTask.resume()
    }
    
    func createImageOnServer_litheen(image: UIImage, completionBlock:((fileURL: NSURL?, error: NSError?)->Void)?) {
        let uuid: String = NSUUID().UUIDString
        let url: NSURL = NSURL(string: "files/media/\(uuid)", relativeToURL: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: url)
        urlRequest.HTTPMethod = "POST"
        
        urlRequest.addValue("multipart/form-data; boundary=\"_PARKLANDS_BOUNDARY_\"", forHTTPHeaderField: "Content-Type")
        
        let boundaryHeader: NSMutableString = NSMutableString()
        boundaryHeader.appendString("--_PARKLANDS_BOUNDARY_\n")
        boundaryHeader.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(uuid)\"\n")
        boundaryHeader.appendString("Content-Type: image/jpg\n\n")
        
        let bodyData: NSMutableData = NSMutableData()
        bodyData.appendData(boundaryHeader.dataUsingEncoding(NSASCIIStringEncoding)!)
        
        // Change UIImage into JPEG data
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        bodyData.appendData(imageData!)
        
        let postTask: NSURLSessionDataTask = self.session.uploadTaskWithRequest(urlRequest, fromData: bodyData) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            // Check HTTP code
            let httpCode: Int = (response as! NSHTTPURLResponse).statusCode
            
            // Receive file url and name
            let json : [String : AnyObject]!
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as! [String : AnyObject]
            } catch let jsonError {
                completionBlock?(fileURL: nil, error: jsonError as NSError)
                return
            }
            
            let fileUrlString: String = json["fileURL"] as! String
            let fileURL: NSURL = NSURL(string: fileUrlString)!
            completionBlock?(fileURL: fileURL, error: nil)
            
            NSLog("HTTP \(httpCode): \(json)")
        }
        
        postTask.resume()
    }
    

}