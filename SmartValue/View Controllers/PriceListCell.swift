//
//  PriceListCell.swift
//  SmartValue
//
//  Created by Jasri Samad on 09/08/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class PriceListCell: UITableViewCell {
//    @IBOutlet weak var productNameLabel: UILabel!
//    @IBOutlet weak var productImage: UIImageView!
//    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var priceDay: UILabel!
    @IBOutlet weak var priceLocation: UILabel!
    @IBOutlet weak var recordedPrice: UILabel!
    
    
    //var loadImageTask: NSURLSessionDataTask?
    
    var price: ProductPriceList! {
        didSet {
            updateDisplay()
        }
    }
//    func displayDate(date: NSDate) -> String {
//        let dayTimePeriodFormatter = NSDateFormatter()
//        dayTimePeriodFormatter.dateFormat = "dd-MM-YYYY"
//        
//        let date: String = dayTimePeriodFormatter.stringFromDate(date)
//        return date
//    }
//    
    func checkPriceAge(date: NSDate) -> String {
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDayForDate(date)
        let date2 = calendar.startOfDayForDate(NSDate())
        
        let flags = NSCalendarUnit.Day
        let components = calendar.components(flags, fromDate: date1, toDate: date2, options: [])
        
        let priceAge = components.day  // This will return the number of day(s) between dates
        var displayAge: String = ""
        switch priceAge {
        case 0:
            displayAge = "today"
        case 1:
            displayAge = "yesterday"
        case 2..<29:
            displayAge = String(priceAge) + " days ago"
        default:
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd-MM-YYYY"
            
            let date: String = dayTimePeriodFormatter.stringFromDate(date)
            displayAge = date
        }
        
        return displayAge
    }
    
    func updateDisplay() {
        self.priceDay.text = checkPriceAge(self.price.date!)
        if self.price.store != nil {
            self.priceLocation.text = self.price.store!
        }
        
        if self.price.price == nil {
            self.recordedPrice.text = "RM 0.00"
        }else {
            self.recordedPrice.text = "RM \(String(format: "%.2f", self.price.price!))"
            
        }
        
        
    }
}