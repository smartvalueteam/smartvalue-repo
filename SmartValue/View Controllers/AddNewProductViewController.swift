//
//  AddNewProductViewController.swift
//  SmartValue
//
//  Created by Jasri Samad on 16/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit
import MobileCoreServices
import BarcodeScanner

protocol AddProductDelegate {
    func viewController(vc: AddNewProductViewController, didAddProduct product: Product!)
}

class AddNewProductViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var barcodeTextField: UITextField!
    @IBOutlet weak var productTextField: UITextField!
    @IBOutlet weak var productPriceTextField: UITextField!
    @IBOutlet weak var AddNewProductCancelBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    enum UIAlertControllerStyle : Int {
        case ActionSheet
        case Alert
    }
    
    var product: Product?
    var newProductBarcode: String?
    var delegate: AddProductDelegate?
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if newProductBarcode != "" {
            barcodeTextField.text = newProductBarcode
        }
        self.productTextField.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewProductViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if productTextField.isFirstResponder() {
            productTextField.resignFirstResponder()
            productPriceTextField.becomeFirstResponder()
        }
        else if productPriceTextField.isFirstResponder() {
            productPriceTextField.resignFirstResponder()
        }
        return true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
//    @IBAction func CancelButton(sender: AnyObject) {
//        self.dismissViewControllerAnimated(true, completion: nil)
//        //self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
    @IBAction func useCamera(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func useCameraRoll(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        imageView.image = image
        //imageView.image = UIImage(data: UIImageJPEGRepresentation(image, 0.1)!) //compress gambar to 10%
        self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func popScanner(sender: AnyObject) {
        let controller = BarcodeScannerController()
        controller.codeDelegate = self
        controller.errorDelegate = self
        controller.dismissalDelegate = self
        
        presentViewController(controller, animated: true, completion: nil)
    }
 
   
    
    
    @IBAction func saveProductByBarButton(sender: AnyObject) {
        self.product = Product(name: self.productTextField.text! )
        self.product?.price = Double(self.productPriceTextField.text!)
        if imageView.image != nil {
            self.product?.photo = imageView.image
        }
        if self.barcodeTextField.text != nil {
            self.product?.barcode = barcodeTextField.text
        }
        
        ProductLoader.sharedLoader.createImageOnServerUsingBESDK(self.product!)
        
       
        self.delegate?.viewController(self, didAddProduct: self.product)

    }
    
    func SaveToUserDefaults(product: Product) {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("SMVProducts") != nil {
            let readProductsData: NSData = defaults.objectForKey("SMVProducts") as! NSData
            var readProduct: [Product] = NSKeyedUnarchiver.unarchiveObjectWithData(readProductsData) as! [Product]
            
            readProduct.append(product)
            
            let newProductsData: NSData = NSKeyedArchiver.archivedDataWithRootObject(readProduct)
            
            defaults.setObject(newProductsData, forKey: "SMVProducts")
            defaults.synchronize()
        }else {
            var initialProductData: [Product] = []
            initialProductData.append(product)
            let newProductsData: NSData = NSKeyedArchiver.archivedDataWithRootObject(initialProductData)
            
            defaults.setObject(newProductsData, forKey: "SMVProducts")
            defaults.synchronize()
        }
        
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "unwindToProductListViewController" {
            self.product = Product(name: self.productTextField.text!)
        }
    }
   
    @IBAction func DismissAddNewProductVC(sender: UIButton) {
        if self.barcodeTextField.text != "" {
            let alertController = UIAlertController(title: "Product not saved.", message: "The scanned product information is not saved yet. \nTap Ignore to discard.", preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
                //print("you have pressed the Cancel button");
               
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Ignore", style: .Default) { (action:UIAlertAction!) in
                //print("you have pressed OK button");
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
            alertController.addAction(OKAction)
            self.presentViewController(alertController, animated: true, completion:nil)

            
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    

}
///////////// BARCODE COCOAPODS //////////////////////////
extension AddNewProductViewController: BarcodeScannerCodeDelegate {
    
    func barcodeScanner(controller: BarcodeScannerController, didCapturedCode code: String) {
        print(code)
        self.barcodeTextField.text = code
        controller.dismissViewControllerAnimated(true, completion: nil)
        self.productTextField.becomeFirstResponder()
    }
}

extension AddNewProductViewController: BarcodeScannerErrorDelegate {
    
    func barcodeScanner(controller: BarcodeScannerController, didReceiveError error: ErrorType) {
        print(error)
    }
}

extension AddNewProductViewController: BarcodeScannerDismissalDelegate {
    
    func barcodeScannerDidDismiss(controller: BarcodeScannerController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
///////////// BARCODE COCOAPODS END //////////////////////////
