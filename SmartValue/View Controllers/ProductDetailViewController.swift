//
//  ProductDetailViewController.swift
//  SmartValue
//
//  Created by Ainor Syahrizal on 28/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    var product: Product!
    var prices: [ProductPriceList] = []
    var productObjectId: String = ""
    var refreshControl: UIRefreshControl!
    var loadImageTask: NSURLSessionDataTask?
    
    @IBOutlet weak var priceListTableView: UITableView!
    
    
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imageProduct.image = UIImage(named: "noimagefound")
        self.imageProduct.contentMode = .ScaleAspectFill
        
        if self.product.photo != nil {
        
            self.imageProduct.image = imageResizeFillUIImage(self.product.photo!, container: self.imageProduct)
        
        } else {
        
            
            loadImageTask = ProductLoader.sharedLoader.loadImageTask(self.product.photoUrl!, completionBlock: { (image, error) -> Void in
                if let error = error {
                    print(error)
                    self.showAlert("Error loading image.", message: error.localizedDescription, okButtonTitle: "Oh well :(")
                } else if let image = image {
                    self.imageProduct.image = image
                    self.product.photo = image
                }
            })
            loadImageTask?.resume()
        }
        
        self.nameProduct.text = self.product.name
        
        if self.product.price != nil {
            self.productPrice.text = "RM \(String(format: "%.2f", self.product.price!))"
        }
        
        // Add in a refreshControl
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.whiteColor()
        self.refreshControl.addTarget(self, action: #selector(ProductDetailViewController.refreshPriceList), forControlEvents: UIControlEvents.ValueChanged)
        self.priceListTableView.addSubview(self.refreshControl)
        
        refreshPriceList()

        let constraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageProduct, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        self.view.addConstraint(constraint)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        if segue.identifier == "showAddPriceVC" {
            let destinationNavigationController = segue.destinationViewController as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! AddNewPriceViewController
            
            targetController.delegate = self

        }
    }
    func imageResizeFillUIImage( image: UIImage, container: UIImageView) -> UIImage {
        
        let bestratio = min(container.frame.size.width/image.size.width,container.frame.size.height/image.size.height)
        
        var newSize: CGSize
        newSize = CGSizeMake(image.size.width * bestratio, image.size.height *  bestratio)
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Do the resizing to the rect using the ImageContext
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
            image.drawInRect(rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func refreshPriceList() {
//        if let refreshControl = self.refreshControl {
//            refreshControl.beginRefreshing() //NOT SURE WHY ERROR IN HERE
//        }
        
        ProductLoader.sharedLoader.loadProductPriceListFromServer(self.product.barcode!, sortBy: "priceAge") { (priceList, error) in
            self.refreshControl.endRefreshing()
            
            if let error = error {
                print(error)
                self.showAlert("Error loading product.", message: error.localizedDescription, okButtonTitle: "Oh well :(")
            } else {
                self.prices = priceList
                
                self.priceListTableView.reloadData()
                
            }
        }

    }

}
extension ProductDetailViewController: AddNewPriceDelegate {
    func viewController(vc: AddNewPriceViewController, didAddPrice price: ProductPriceList!) {
        //code
        //self.prices.append(price)
        price.barCode = self.product.barcode
        ProductLoader.sharedLoader.addPriceOnServer(price) { (succeeded, error) in
            if let error = error {
                print(error)
                self.showAlert("Error loading product pricelist.", message: error.localizedDescription, okButtonTitle: "Oh well :(")
            } else {
                //reload table view
                self.refreshPriceList()
            }
            
            
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        //self.priceListTableView.reloadData()
    }
}


extension ProductDetailViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return prices.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: PriceListCell = self.priceListTableView.dequeueReusableCellWithIdentifier("priceListCell", forIndexPath: indexPath) as! PriceListCell
        let price: ProductPriceList = self.prices[indexPath.row]
        
        cell.price = price
        
        return cell
        
    }
}

extension ProductDetailViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
    }
}

