//
//  AddNewPriceViewController.swift
//  SmartValue
//
//  Created by Jasri Samad on 10/08/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit
import MobileCoreServices

protocol AddNewPriceDelegate {
    func viewController(vc: AddNewPriceViewController, didAddPrice product: ProductPriceList!)
}

class AddNewPriceViewController: UIViewController, UINavigationControllerDelegate {
    
    var price: ProductPriceList?
    var delegate: AddNewPriceDelegate?
    
    @IBOutlet weak var newPriceTextField: UITextField!
    @IBOutlet weak var newPriceLocation: UITextField!
    
    
    
    @IBAction func cancelAddNewPrice(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveNewPrice(sender: AnyObject) {
        
        self.price = ProductPriceList(name: "" )
        
        if newPriceTextField.text != nil {
            self.price?.price = Double(newPriceTextField.text!)
        }
        
        self.price?.date = NSDate()
        
        if newPriceLocation.text != nil {
            self.price?.store = newPriceLocation.text
        }
        //ProductLoader.sharedLoader.createImageOnServerUsingBESDK(self.product!)
        
        self.delegate?.viewController(self, didAddPrice: self.price)
    }
}