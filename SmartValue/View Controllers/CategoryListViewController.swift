//
//  CategoryListViewController.swift
//  SmartValue
//
//  Created by Jasri Samad on 18/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class CategoryListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var products: [Product] = []
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Add in a refreshControl
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(CategoryListViewController.refreshProduct), forControlEvents: UIControlEvents.ValueChanged)
        self.productCollectionView.addSubview(self.refreshControl)
        
        self.refreshProduct()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let categoryCell: CategoryCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("categoryCell", forIndexPath: indexPath) as! CategoryCollectionViewCell
        
        let product: Product = self.products[indexPath.row]
        
        categoryCell.product = product
        
        return categoryCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        print("user tapped on \(indexPath.row)")
    }
    
    func refreshProduct() {
        if let refreshControl = self.refreshControl {
            refreshControl.beginRefreshing()
        }
        
        ProductLoader.sharedLoader.loadProductsFromServer (0,productBarCode: "",productNameSearch: ""){ (products, offset, error) -> Void in
            self.refreshControl.endRefreshing()
            if let error = error {
                print(error)
                self.showAlert("Error", message: error.localizedDescription, okButtonTitle: "Oh well :(")
            } else {
                
                self.products = products
                self.productCollectionView.reloadData()
            }
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
