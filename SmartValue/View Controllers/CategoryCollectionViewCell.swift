//
//  CategoryCollectionViewCell.swift
//  SmartValue
//
//  Created by Jasri Samad on 15/08/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    var loadImageTask: NSURLSessionDataTask?
    
    var product: Product! {
        didSet {
            updateDisplay()
        }
    }
    
    
    func updateDisplay() {
        self.categoryLabel.text = self.product.name
        
        if let image = self.product.photo {
            self.categoryImageView.image = image
            
        } else if let photoURL = self.product.photoUrl {
            //self.loadImageTask?.cancel()
            self.categoryImageView.image = UIImage(named: "newproduct")
            self.product.photo = UIImage(named: "newproduct")
            
            self.loadImageTask = ProductLoader.sharedLoader.loadImageTask(photoURL, completionBlock: { (image, error) -> Void in
                if let error = error {
                    print(error)
                    self.showAlert("Error loading image \n[product Cell]", message: error.localizedDescription, okButtonTitle: "Oh well :(")
                }
                
                if let image = image {
                    self.categoryImageView.image = image
                    self.product.photo = image
                }
            })
            self.loadImageTask?.resume()
            
        }else {
            self.categoryImageView.image = UIImage(named: "newproduct")
            self.product.photo = UIImage(named: "newproduct")
        }
    }
}
