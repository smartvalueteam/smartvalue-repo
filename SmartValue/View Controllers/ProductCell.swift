//
//  ProductCell.swift
//  SmartValue
//
//  Created by Jasri Samad on 30/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    
    var loadImageTask: NSURLSessionDataTask?
    
    var product: Product! {
        didSet {
            updateDisplay()
        }
    }
    func imageResizeFillUIImage( image: UIImage, container: UIImageView) -> UIImage {
        container.contentMode = .ScaleAspectFill
        let bestratio = min(container.frame.size.width/image.size.width,container.frame.size.height/image.size.height)
        
        var newSize: CGSize
        newSize = CGSizeMake(image.size.width * bestratio, image.size.height *  bestratio)
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Do the resizing to the rect using the ImageContext
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    
    func updateDisplay() {
        self.productNameLabel.text = self.product.name
        
        if self.product.price == nil {
            self.productPrice.text = "RM 0.00"
        }else {
            self.productPrice.text = "RM \(String(format: "%.2f", self.product.price!))"
            
        }

        if let image = self.product.photo {
            self.productImage.image = self.imageResizeFillUIImage(image,container: self.productImage)
            
        } else if let photoURL = self.product.photoUrl {
            //self.loadImageTask?.cancel()
            self.productImage.image = UIImage(named: "newproduct")
            
            self.loadImageTask = ProductLoader.sharedLoader.loadImageTask(photoURL, completionBlock: { (image, error) -> Void in
                if let error = error {
                    print(error)
                    self.showAlert("Error loading image \n[product Cell]", message: error.localizedDescription, okButtonTitle: "Oh well :(")
                }
                
                if let image = image {
                    self.productImage.image = self.imageResizeFillUIImage(image,container: self.productImage)
                    self.product.photo = image
                }
            })
            self.loadImageTask?.resume()
            
        }else if self.productImage.image != self.product.photo {
            self.productImage.image = self.imageResizeFillUIImage(self.product.photo!,container: self.productImage)
        }else {
            self.productImage.image = UIImage(named: "newproduct")
            
        }
    }
}
