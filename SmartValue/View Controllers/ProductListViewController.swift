//
//  ProductListViewController.swift
//  SmartValue
//
//  Created by Jasri Samad on 16/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import UIKit
import BarcodeScanner

class ProductListViewController: UIViewController, UISearchBarDelegate, UIScrollViewDelegate{
    enum UIAlertControllerStyle : Int {
        case ActionSheet
        case Alert
    }
    
    
    @IBOutlet weak var ProductTableView: UITableView!
    @IBOutlet weak var searchTextField: UISearchBar!
    @IBOutlet weak var productListLabel: UILabel!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var loadingData = false
    var refreshPage = 1

    var refreshControl: UIRefreshControl!

    var products: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        
        // Add in a refreshControl
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: #selector(ProductListViewController.refreshProductSelector), forControlEvents: UIControlEvents.ValueChanged)
        self.ProductTableView.addSubview(self.refreshControl)
        
        self.refreshProduct()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //SEARCH BAR RELATED
    
    // Do a search from database as user type in the product name. Immitate autocomplete on browser.
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            searchBar.showsCancelButton = true
            ProductLoader.sharedLoader.loadProductsFromServer (0,productBarCode: "",productNameSearch: searchText){ (products, offset, error) -> Void in
                self.refreshControl.endRefreshing()
                if let error = error {
                    print(error)
                    self.showAlert("Error", message: error.localizedDescription, okButtonTitle: "Oh well :(")
                } else {
                    if products.count == 0 {
                        self.productListLabel.text = "No product found"
                    }else{
                        self.productListLabel.text = "\(products.count) product(s) found"
                    }
                    
                    self.products = products
                    self.ProductTableView.reloadData()
                }
            }
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar){
        print("search" + searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        self.refreshProduct()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // LOAD MORE PRODUCT
    
    //Check if the scroller reach he bottom - load more
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = CGFloat(offset.y + bounds.size.height - inset.bottom)
        let h = CGFloat(size.height)
        
        let reload_distance = CGFloat(10)
        if(y > (h + reload_distance)) {
            if self.products.count >= 10 {
                self.spinner.startAnimating()
                //print("load more rows , offset \(self.products.count)")
                self.refreshProduct(self.products.count)
            }
        }
    }
    
    
    
    @IBAction func popScanner(sender: AnyObject) {
        let controller = BarcodeScannerController()
        controller.codeDelegate = self
        controller.errorDelegate = self
        controller.dismissalDelegate = self
        
        presentViewController(controller, animated: true, completion: nil)
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToProductListViewController(segue: UIStoryboardSegue) {
        if let addProductVC: AddNewProductViewController = segue.sourceViewController as? AddNewProductViewController {
            if let product = addProductVC.product {
                self.products.append(product)
                self.ProductTableView.reloadData()
            }
        }
        print("Unwinding here")
    }
    
    func loadFromUserDefaults() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("SMVProducts") != nil {
            let readProductsData: NSData = defaults.objectForKey("SMVProducts") as! NSData
            
            if let readProduct: [Product] = NSKeyedUnarchiver.unarchiveObjectWithData(readProductsData) as? [Product] {
                for i in 0..<readProduct.count {
                    self.products.append(readProduct[i])
                }
                
            }
        }
    }
    func refreshProductSelector() {
        // not sure why the selector call refreshProduct with no param 
        // create offSet value of 140461701785008 eventhough param default is 0
        // this func solve it, but not sure why it happen.
        self.refreshProduct()
    }
    
    func refreshProduct(offSet: Int = 0) {
        if let refreshControl = self.refreshControl {
            
            refreshControl.beginRefreshing()
        }

        ProductLoader.sharedLoader.loadProductsFromServer (offSet,productBarCode: "",productNameSearch: ""){ (products,offset, error) -> Void in
            self.refreshControl.endRefreshing()
            if let error = error {
                print(error)
                self.showAlert("Error", message: error.localizedDescription, okButtonTitle: "Oh well :(")
            } else {
                //Backendless default pageSize = 10
                let pageSize: Int = 10
                //Only run if product count is more or equal to pageSize, meaning not the first time load.
                if self.products.count == offset && self.products.count >= pageSize {
                    
                    for Product in products {
                        self.products.append(Product)
                    }
                    self.productListLabel.text = "Featured Items"
                    self.ProductTableView.reloadData()
                    self.spinner.stopAnimating()
                    self.loadingData = false
                    //print("offset:\(offset) product:\(self.products.count)")
                    
                } else if offset == 0 {
                    //first time product loading or reloading
                    self.productListLabel.text = "Featured Items"
                    self.products = products
                    self.ProductTableView.reloadData()
                }
                
            }
        }
    }
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
  
        if segue.identifier == "showProductDetailVC" {
            
            let destVC: ProductDetailViewController = segue.destinationViewController as! ProductDetailViewController
            
            if let selectedIndexPath = self.ProductTableView.indexPathForSelectedRow {
                let product: Product = self.products[selectedIndexPath.row]
                destVC.product = product
            }
        }
        
        if segue.identifier == "showAddProductVC" {
            let destVC: AddNewProductViewController = segue.destinationViewController as! AddNewProductViewController
            
            destVC.delegate = self
        }
    }
    

}

///////////// BARCODE COCOAPODS //////////////////////////
extension ProductListViewController: BarcodeScannerCodeDelegate {
    
    func barcodeScanner(controller: BarcodeScannerController, didCapturedCode code: String) {
        print(code)
        //do product search here using barcode received here
        var foundProduct: Bool = false
        if code != "" {
            if let refreshControl = self.refreshControl {
                refreshControl.beginRefreshing()
            }
            
            ProductLoader.sharedLoader.loadProductsFromServer(0,productBarCode: code,productNameSearch: "", completionBlock: { (products: [Product], offset, error: NSError?) in
                self.refreshControl.endRefreshing()
                if let error = error {
                    print(error)
                    self.showAlert("Error", message: error.localizedDescription, okButtonTitle: "Oh well :(")
                    
                } else if let product: Product = products.first {
                    foundProduct = true
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let detailVC = storyboard.instantiateViewControllerWithIdentifier("ProductDetailViewController") as! ProductDetailViewController
                    detailVC.product = product
                    
                    controller.dismissViewControllerAnimated(true, completion: nil)
                    self.navigationController?.pushViewController(detailVC, animated: true)
                    
                    
//                    self.products = products
//                    
//                    self.ProductTableView.reloadData()
                }

            })
        }
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(6 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            //controller.resetWithError("The product is not in our database. Please save the details in the form.")
            
            
            //if product not found
            if foundProduct == false {
                let alertController = UIAlertController(title: "Product not found", message: "Sorry, no such product in our database. \nTap on Add button to add this product to database or Cancel to scan again.", preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
                    print("you have pressed the Cancel button");
                    controller.reset()
                }
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "Add", style: .Default) { (action:UIAlertAction!) in
                    //Product Barcode not found, create new entry
                    
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let addNewProductVC = storyboard.instantiateViewControllerWithIdentifier("AddNewProductViewController") as! AddNewProductViewController
                    addNewProductVC.newProductBarcode = code
                    addNewProductVC.delegate = self
                    
                    controller.dismissViewControllerAnimated(true, completion: nil)
                    self.navigationController?.presentViewController(addNewProductVC, animated: true, completion: nil)
                    
                    //self.navigationController?.pushViewController(addNewProductVC, animated: true)
                    
                }
                alertController.addAction(OKAction)
                controller.presentViewController(alertController, animated: true, completion:nil)
            } else {
                controller.dismissViewControllerAnimated(true, completion: nil)
            }
            
            
        }
    }
}

extension ProductListViewController: BarcodeScannerErrorDelegate {
    
    func barcodeScanner(controller: BarcodeScannerController, didReceiveError error: ErrorType) {
        print(error)
    }
}

extension ProductListViewController: BarcodeScannerDismissalDelegate {
    
    func barcodeScannerDidDismiss(controller: BarcodeScannerController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
///////////// BARCODE COCOAPODS END //////////////////////////




extension ProductListViewController: AddProductDelegate {
    func viewController(vc: AddNewProductViewController, didAddProduct product: Product!) {
        //code
        self.products.append(product)
        self.dismissViewControllerAnimated(true, completion: nil)
        
        //self.refreshPark()
        self.ProductTableView.reloadData()
    }
}

extension ProductListViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return products.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: ProductCell = self.ProductTableView.dequeueReusableCellWithIdentifier("productCell", forIndexPath: indexPath) as! ProductCell
        let product: Product = self.products[indexPath.row]
        
        cell.product = product
        
        return cell

    }
}

extension ProductListViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        let detailsVC: ProductDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailViewController") as! ProductDetailViewController
//        
//        let product: Product = self.products[indexPath.row]
//        detailsVC.product = product
//        
//        self.navigationController?.pushViewController(detailsVC, animated: true)
        
    }
}

