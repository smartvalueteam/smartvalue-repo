//
//  ScannerViewController.swift
//  SmartValue
//
//  Created by Jasri Samad on 31/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//  www.hackingwithswift.com/example-code/media/how-to-scan-a-barcode

import AVFoundation
import UIKit

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.blackColor()
        captureSession = AVCaptureSession()
        
        let videoCaptureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed();
            return;
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypePDF417Code]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        previewLayer.frame = view.layer.bounds;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        view.layer.addSublayer(previewLayer);
        
        //Manually add cancel button in scanner view
        let scannerCancelButton: UIButton = UIButton(frame: CGRectMake(100, 400, 100, 50))
        
        scannerCancelButton.backgroundColor = UIColor.darkGrayColor()
        scannerCancelButton.setTitle("Cancel", forState: UIControlState.Normal)
        scannerCancelButton.addTarget(self, action: #selector(ScannerViewController.cancelButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        scannerCancelButton.tag = 1               // change tag property
        self.view.addSubview(scannerCancelButton) // add to view as subview
        
        let scannerInstruction: UILabel = UILabel(frame: CGRectMake(15,300,300,100))
        scannerInstruction.text = "Please tap on the barcode to focus"
        scannerInstruction.textColor = UIColor.whiteColor()
        self.view.addSubview(scannerInstruction)
        
        captureSession.startRunning();
    }
    
    func cancelButtonAction(sender: UIButton!) {
        let buttonSendTag: UIButton = sender
        if buttonSendTag.tag == 1 {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .Alert)
        ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(ac, animated: true, completion: nil)
        captureSession = nil
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.running == false) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.running == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
            
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            foundCode(readableObject.stringValue);
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func foundCode(code: String) {
        print(code)
        Barcode.sharedInstance.scannedCode = code
        
    }
    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait
    }
}

