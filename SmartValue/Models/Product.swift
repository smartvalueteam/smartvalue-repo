//
//  Product.swift
//  SmartValue
//
//  Created by Dwayne Ong on 19/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import Foundation
import UIKit


class Product: NSObject, NSCoding {
    
    enum Category: String {
        case Unknown
        case Dairy
        case Household
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.name, forKey: "SMVProductName")
        keyedArchiver.encodeObject(self.price, forKey: "SMVProductPrice")
        
        if self.photo != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.photo!)
            keyedArchiver.encodeObject(imageData, forKey: "SMVProductPhoto")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnArchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyedUnArchiver.decodeObjectForKey("SMVProductName") as! String
        self.init(name: name)
        
        self.price = keyedUnArchiver.decodeObjectForKey("SMVProductPrice") as? Double
        self.location = keyedUnArchiver.decodeObjectForKey("SMVProductLocation") as? String
        if let imageData = keyedUnArchiver.decodeObjectForKey("SMVProductPhoto") as? NSData {
            self.photo = UIImage(data: imageData)
        }
        
    }
    var name: String
    var objectId: String?
    
    var barcode: String?
    
    var price: Double?
    var photo: UIImage?
    var photoUrl: NSURL?
//    var photoFilename: String? {
//        get {return self.photoUrl?.pathComponents?.last}
//    }
    var location: String?
    //var volume:Double?
    //var weight: Double?
    //var category: Category = .Unknown
    
    init(name: String) {
        self.name = name

        super.init()
    }
    
     

    
    class func createRandomProduct() -> Product {
        let dataset: [String] = ["Sos Cili Adabi", "Sardin Cap Ayam", "Maggi Kari", "Susu Dutch Lady", "Kicap Manis"]
        let index: Int = Int(arc4random_uniform(UInt32(dataset.count)))
        let randomProduct: String = dataset[index]
        
        let product: Product = Product(name: randomProduct)
        product.photo = UIImage(named: randomProduct)
        product.price = Double(arc4random() % 20)
        return product
    }
    
    

}

extension Product {
    convenience init?(json: [String: AnyObject]) {
        let name: String = json["productName"] as! String
        self.init(name:name)
        
        updateWithJson(json)
    }
    
    func updateWithJson(json: [String: AnyObject]) {
        if let name = json["productName"] as? String {
            self.name = name
            
            if let price = json["productPrice"] as? Double {
                self.price = price
            }
            if let barcode = json["Barcode"] as? String {
                self.barcode = barcode
            }
            
            if let objectId: String = json["objectId"] as? String {
                self.objectId = objectId
            }
            
            if json["productPhoto"] != nil {
                if let photoUrl = json["productPhoto"] as? String {
                    self.photoUrl = NSURL(string: photoUrl)
                }
                
            }

        }
        //
//        if let photoDict: [String:AnyObject] = json["productPhoto"] as? [String: AnyObject] {
//            //if let photoUrlString: String = photoDict["productPhotoUrl"] as? String {
//                self.photoUrl = NSURL(string: json["productPhoto"] as! String)
//            //}
//        }
    }
    
    func jsonDict() -> [String:AnyObject] {
        let jsonDict: [String:AnyObject] = [
            "productName":self.name,
            "productPrice": self.price ?? NSNull(),
            "Barcode": self.barcode ?? NSNull(),
            "productPhoto": String( self.photoUrl ?? NSNull())]
        
        
        //if let _ = self.photoUrl {
            //jsonDict["productPhoto"] = ["name": self.photoFilename!, "__type": "File"]
            
        //}
        
        return jsonDict
    }
}







