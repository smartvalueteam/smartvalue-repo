//
//  Store.swift
//  SmartValue
//
//  Created by Ainor Syahrizal on 21/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import Foundation
import UIKit

class Store {
    var id: String?
    var name: String
    var photo: UIImage?
    var location: String?
    
    init(name: String) {
        self.name = name
    }
    
    class func generateFakeStores() -> [Store] {
        let store1: Store = Store(name: "Tesco Ampang")
        let store2: Store = Store(name: "Aeon Wangsa Maju")
        let store3: Store = Store(name: "Giant Batu Caves")
        let store4: Store = Store(name: "Econsave Klang")
        
        let stores: [Store] = [store1, store2, store3, store4]
        
        return stores
    }
}