//
//  ProductPrice.swift
//  SmartValue
//
//  Created by Jasri Samad on 09/08/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import Foundation
import UIKit

class ProductPriceList: NSObject, NSCoding {
    var productObjectId: String?
    var name: String
    var barCode: String?
    var price: Double?
    var store: String?
    var date: NSDate?
    
    init(name: String) {
        self.name  = name
        
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.productObjectId, forKey: "SMVProductObjectId")
        keyedArchiver.encodeObject(self.price, forKey: "SMVProductPrice")
        keyedArchiver.encodeObject(self.store, forKey: "SMVProductStore")
        keyedArchiver.encodeObject(self.date, forKey: "SMVProductDate")
        keyedArchiver.encodeObject(self.barCode, forKey: "SMVProductBarCode")
        keyedArchiver.encodeObject(self.name, forKey: "SMVProductName")
        }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnArchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyedUnArchiver.decodeObjectForKey("SMVProductName") as! String
        self.init(name: name)
        
        self.productObjectId = keyedUnArchiver.decodeObjectForKey("SMVProductObjectId") as? String
        self.price = keyedUnArchiver.decodeObjectForKey("SMVProductPrice") as? Double
        self.store = keyedUnArchiver.decodeObjectForKey("SMVProductStore") as? String
        self.date = keyedUnArchiver.decodeObjectForKey("SMVProductDate") as? NSDate
        self.barCode = keyedUnArchiver.decodeObjectForKey("SMVProductBarCode") as? String
        }

}

extension ProductPriceList {
    convenience init?(json: [String: AnyObject]) {
        //let name: String = json["productName"] as! String
        self.init(name:"pricelistonly")
        
        updateWithJson(json)
    }
    
    func getDateFromString(date: Double?) -> NSDate? {
        let foo: NSTimeInterval = date!/1000
        let theDate = NSDate(timeIntervalSince1970: foo)
        
        return theDate
    }
    
    func updateWithJson(json: [String: AnyObject]) {
//        if let name = json["productName"] as? String {
//            self.name = name
//        }
        if let productObjectId = json["objectId"] as? String {
            self.productObjectId = productObjectId
        }
        if let price = json["productPrice"] as? Double {
            self.price = price
        }
        if let store = json["productStore"] as? String {
            self.store = store
        }
        if let barCode = json["Barcode"] as? String {
            self.barCode = barCode
        }
        let date = getDateFromString((json["created"] as! Double))
        
        if date != nil {
            self.date = date
        }
        
    }
    
    func jsonDictPricelist() -> [String:AnyObject] {
        let jsonDict: [String:AnyObject] = [
//            "name":self.name,
            "productPrice": self.price ?? NSNull(),
            "productStore": self.store ?? NSNull(),
            "Barcode": self.barCode ?? NSNull()]
        
                return jsonDict
    }
}