//
//  ScannedCode.swift
//  SmartValue
//
//  Created by Jasri Samad on 31/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import Foundation

//Singleton for barcode scanner
class Barcode {
    static let sharedInstance = Barcode()
    var scannedCode: String = ""
    
    private init(){}
    
    func clearBarcode() {
        self.scannedCode = ""
    }
}
