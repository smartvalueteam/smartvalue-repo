//
//  User.swift
//  SmartValue
//
//  Created by Dwayne Ong on 23/07/2016.
//  Copyright © 2016 Spektroniaga Venture. All rights reserved.
//

import Foundation
import UIKit

class User {
    var id: String?
    var name: String?
    var email: String?
    var contactNo: String?
    var age: Int?
    var gender: String?
    var photo: UIImage?
    var location: String?
    
    init(name: String, email: String, contactNo: String) {
        self.name = name
        self.email = email
        self.contactNo = contactNo
    }
}